//
//  ResultCollectionViewController.swift
//  Rhymer
//
//  Created by Arvind Ravi on 17/11/15.
//  Copyright © 2015 Dryfish. All rights reserved.
//

import UIKit
import SwiftyJSON

private let reuseIdentifier = "Cell"

class ResultCollectionViewController: UICollectionViewController, APIProtocol, UICollectionViewDelegateFlowLayout {
  
  // MARK: Constants
  struct Constants {
    static let CopySuccessLabel = "Copied word to clipboard!"
    
    struct Colors {
      static let DarkGrayColor = UIColor(netHex:0x272F32)
      static let BrightRedColor = UIColor(netHex:0xFF3D2E)
      static let CloudsColor = UIColor(netHex: 0xDAEAEF)
    }
    
    struct TextAttributes {
      static let CellText: [ String : AnyObject ] = [
        NSForegroundColorAttributeName : Colors.BrightRedColor,
        NSKernAttributeName : (2.5)
      ]
      
      static let NavigationBar: [ String : AnyObject ] = [
        NSForegroundColorAttributeName : Colors.CloudsColor,
        NSKernAttributeName : (2.5)
      ]
    }
  }
  
  // MARK: Vars
  var word: String?
  var results = [String]()
  var numberOfRows = 0
  var wordBuffer: String!
  
  // MARK: Outlets
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  // MARK: Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    configureCollectionView()
    API.delegate = self
    
    // Remove spacing above the first cell
    self.automaticallyAdjustsScrollViewInsets = false
    self.collectionView?.contentInset = UIEdgeInsetsMake(65, 0, 0, 0)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    customizeNavBar()

    if let word = word {
      self.navigationItem.title = word
      API.getWordsThatRhymeWith(word)
    }
  }
  
  func customizeNavBar() {
    self.navigationController?.navigationBar.titleTextAttributes = Constants.TextAttributes.NavigationBar
    self.navigationController?.navigationBar.barTintColor = Constants.Colors.DarkGrayColor
    self.navigationController?.navigationBar.tintColor = Constants.Colors.CloudsColor
  }
  
  func configureCollectionView() {
    collectionView?.dataSource = self
    collectionView?.delegate = self
    collectionView?.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)
  }
  
  
  // MARK: API Delegate Method
  func didReceiveResult(results: JSON) {
    let rhymes = results["rhymes"]["all"]
    for word in rhymes {
      if let wordString = word.1.string {
        self.results.append(wordString)
      }
    }
    if (self.results.count > 0) {
      self.spinner.stopAnimating()
      self.numberOfRows = self.results.count
      self.collectionView?.reloadData()
    } else {
      handleAlert() 
    }
  }
  
  internal func handleAlert() {
    let alert = UIAlertController(title: "Oh Oh!", message: "No Rhyming Words found!", preferredStyle: .Alert)
    let goBackAction = UIAlertAction(title: "Go Back", style: .Default) { action in
      self.navigationController?.popViewControllerAnimated(true)
    }
    alert.addAction(goBackAction)
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
  // MARK: UICollectionViewDataSource
  
  override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.numberOfRows
  }
  
  override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ResultCollectionViewCell
    
    // Configure the cell
    if self.results.count > 0 {
      let word = self.results[indexPath.row]
      cell.textLabel.attributedText = NSAttributedString(string: word, attributes: Constants.TextAttributes.CellText)
      cell.backgroundColor = Constants.Colors.DarkGrayColor
    }
    
    // Shadow
    cell.layer.masksToBounds    = false
    cell.layer.contentsScale    = UIScreen.mainScreen().scale
    cell.layer.shadowOpacity    = 0.75
    cell.layer.shadowOffset     = CGSizeZero
    cell.layer.shadowRadius     = 2
    cell.layer.shadowPath       = UIBezierPath(rect: cell.bounds).CGPath
    
    return cell
  }
  
  // MARK: UICollectionViewDelegate
  
  // Select
  override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }
  
  override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    handleSelectionForCellAtIndexPath(indexPath)
  }
  
  
  enum CellState {
    case Select
    case Deselect
  }
  
  func customizeCellAtIndexPath(indexPath: NSIndexPath, cellState: CellState) {
    if let cell = collectionView?.cellForItemAtIndexPath(indexPath) as? ResultCollectionViewCell {
      // Customize
      if cellState == .Select {
        cell.backgroundColor = Constants.Colors.CloudsColor
      } else {
        cell.backgroundColor = Constants.Colors.DarkGrayColor
        cell.textLabel.attributedText = NSAttributedString(string: self.wordBuffer, attributes: Constants.TextAttributes.CellText)
      }
    }
  }
  
  func handleSelectionForCellAtIndexPath(indexPath: NSIndexPath) {
    if let cell = collectionView?.cellForItemAtIndexPath(indexPath) as? ResultCollectionViewCell {
      if cell.textLabel.text != Constants.CopySuccessLabel {
        customizeCellAtIndexPath(indexPath, cellState: .Select)
        self.wordBuffer = cell.textLabel.text
        UIPasteboard.generalPasteboard().string = self.wordBuffer
        cell.textLabel.text = Constants.CopySuccessLabel
        cell.textLabel.attributedText = NSAttributedString(string: Constants.CopySuccessLabel, attributes: Constants.TextAttributes.CellText)
      } else {
        customizeCellAtIndexPath(indexPath, cellState: .Deselect)
      }
    }
  }
  
  // Deselect
  override func collectionView(collectionView: UICollectionView, shouldDeselectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }
  
  override func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    customizeCellAtIndexPath(indexPath, cellState: CellState.Deselect)
  }
  
  // Size
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    let collectionViewWidth = self.collectionView?.bounds.size.width
    return CGSize(width: collectionViewWidth!, height: 133)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
}
