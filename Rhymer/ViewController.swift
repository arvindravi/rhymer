//
//  ViewController.swift
//  Rhymer
//
//  Created by Arvind Ravi on 17/11/15.
//  Copyright © 2015 Arvind Ravi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
  
  
  // MARK: Constants
  struct Constants {
    static let ResultSegueIdentifier = "ShowResultCollectionSegue"
    static let PlaceholderText = "Enter a word"
    static let PlaceholderAttributes: [String:AnyObject] = [
      NSForegroundColorAttributeName: UIColor(white: 1, alpha: 0.7),
      NSKernAttributeName: (7.0)
    ]
    static let TextFieldAttributes: [String:AnyObject] = [
      NSForegroundColorAttributeName: UIColor.whiteColor(),
      NSKernAttributeName: (7.0),
    ]
    static let NAAlertMessage = "Please make sure you have an active internet connection."
  }
  
  // MARK: Outlets
  @IBOutlet weak var wordTextField: TextField! {
    didSet {
      wordTextField.delegate = self
      wordTextField.tintColor = UIColor.whiteColor()
    }
  }
  
  @IBOutlet weak var rhymeButton: UIButton!
  
  // MARK: IBActions
  @IBAction func goPressed(sender: UIButton) {
    handleWord()
  }
  
  func handleWord() {
    if isEmpty(wordTextField) {
      handleAlert()
    } else {
      self.performSegueWithIdentifier(Constants.ResultSegueIdentifier, sender: nil)
    }
  }
  
  func handleAlert() {
    let alert = UIAlertController(title: "Oh wait!", message: "You cannot rhyme nothing. \n Please enter a word!", preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  // MARK: Validation
  internal func isEmpty(textField: UITextField) -> Bool {
    if textField.text == nil || textField.text == "" {
      return true
    } else {
      return false
    }
  }
  
  // Result Array
  var rhymes = [String]()
  
  // MARK: Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    
    handleNetworkAccess()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.navigationBarHidden = true
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(true)
    self.navigationController?.navigationBarHidden = false
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    // UITextField Border Bottom
    let border = CALayer()
    let width = CGFloat(2.0)
    let textField = self.wordTextField
    border.borderColor = UIColor.whiteColor().CGColor
    border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
    
    border.borderWidth = width
    textField.layer.addSublayer(border)
    textField.layer.masksToBounds = true
    textField.defaultTextAttributes = Constants.TextFieldAttributes
    textField.textAlignment = NSTextAlignment.Center
    
    // Placeholder
    textField.attributedPlaceholder = NSAttributedString(string: Constants.PlaceholderText, attributes: Constants.PlaceholderAttributes)
  }
  
  func handleNetworkAccess() {
    if (Reachability.isConnectedToNetwork() == false) {
      let alert = UIAlertController(title: "Oops!", message: Constants.NAAlertMessage, preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
      self.presentViewController(alert, animated: true, completion: nil)
    }
  }
  
  // MARK: Keyboard Methods
  // Resign Keyboard on touch
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    view.endEditing(true)
  }
  
  // MARK: Segue
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let identifier = segue.identifier {
      if identifier == Constants.ResultSegueIdentifier {
        if let resultsCVC = segue.destinationViewController as? ResultCollectionViewController {
          if let word = wordTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) {
            resultsCVC.word = word
          }
        }
      }
    }
  }
  
  // MARK: Hide Status Bar
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
  
  // MARK: Text Field Delegates
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    handleWord()
    return true
  }
  
}

