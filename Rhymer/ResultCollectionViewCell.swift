//
//  ResultCollectionViewCell.swift
//  Rhymer
//
//  Created by Arvind Ravi on 18/11/15.
//  Copyright © 2015 Dryfish. All rights reserved.
//

import UIKit

class ResultCollectionViewCell: UICollectionViewCell {
  
  // MARK: Outlets
  @IBOutlet weak var textLabel: UILabel!

}
