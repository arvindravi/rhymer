//
//  APIProtocol.swift
//  Rhymer
//
//  Created by Arvind Ravi on 17/11/15.
//  Copyright © 2015 Dryfish. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol APIProtocol {
  func didReceiveResult(results: JSON)
}