//
//  API.swift
//  Rhymer
//
//  Created by Arvind Ravi on 17/11/15.
//  Copyright © 2015 Dryfish. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class API {
  static var delegate: APIProtocol?
  
  class func getWordsThatRhymeWith(word: String) -> Void {
    // Request Headers
    var headers = [String:String]()
    headers["X-Mashape-Key"] = "7unDBhuCwbmshonleIxEHzMOCs08p1v1ZQtjsnlzWens4TnGjq"
    headers["Accept"] = "application/json"
    
    // Request URL
    let url = "https://wordsapiv1.p.mashape.com/words/\(word)/rhymes"
    Alamofire.request(.GET, url, headers: headers).responseJSON { response in
      let data = response.result
      if data.error != nil {
        print(data.error?.description)
      }
      if let result = response.result.value {
        print(JSON(result))
        if self.delegate != nil {
          self.delegate!.didReceiveResult(JSON(result))
        }
      }
    }
  }
}